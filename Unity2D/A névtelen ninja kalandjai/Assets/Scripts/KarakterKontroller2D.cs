﻿using UnityEngine;
using System.Collections;

public class KarakterKontroller2D : MonoBehaviour
{

	private const float skinWidth = .02f;						//"bőr" szélessége
	private const int osszVRay = 8;								//összes ray amit majd kilő(vízszintes)
	private const int osszFRay = 4;								//összes ray amit majd kilő(függőleges)
	private Vector2 _sebesseg;									//ez azért kell mer a sima sebesség property-t nem lehet közvetlenül változtatni mivel a vector property-vel tér vissza
	private Transform _transform;								//ezek 3an alliasok kb ugyanazért mint a fentebbi _sebesség
	private Vector3 _localScale;
	private BoxCollider2D _boxCollider;
	private KontrollerParameterek2D _parameterOverride;
	private float ugriJarJar;
	private Vector3 globalPontPl;
	private Vector3 helyPontPl;
	private GameObject utolsoAlldogalas;
	private float 
		rayFTav,
		rayVTav;
	private Vector3
		_raycastBalFent,
		_raycastBalLent,
		_raycastJobbLent;

	public LayerMask platfomMaszk;								//ez arra kell, hogy definiáljuk, hogy milyen layerekkel akarunk ütközni(maszkolás)
	public KontrollerParameterek2D defaultParameterek;			//ez kell a paraméterekhez
	public KontrollerAllas2D allas {get; private set;}          //különböző állásokhoz
	public Vector2 sebesseg { get{ return _sebesseg; }}

	public bool utkozesKezeles{ get; set;}
	public KontrollerParameterek2D parameterek{get{return _parameterOverride ?? defaultParameterek;}}	//ez avgy az overRideos paraméterekkel fog visszatérni vagy amennyiben ez null akkor a degfault-al
	public GameObject allokVmin{get; private set;}				//ez a mozgó platformok miatt is fontos
	public Vector3 platformSebesseg;//ez majd lelesz kérdezve

	public bool ugorhat
	{
		get
		{ 

			if (parameterek.ugrasKorlat == KontrollerParameterek2D.UgrasViselkedes.UgrasBarhol)
				return ugriJarJar <= 0; //true lesz ha au ugriJarJar kiseb mint 0



			if (parameterek.ugrasKorlat == KontrollerParameterek2D.UgrasViselkedes.UgrasFold)
				return allas.Foldon;    //ha földön vagyunk akkor lesz true

			return false;

		}

	}
	//inicializálások főleg
	public void Awake()
	{
		utkozesKezeles = true;
		allas = new KontrollerAllas2D ();
		_transform = transform;
		_localScale = transform.localScale;
		_boxCollider = GetComponent<BoxCollider2D> ();

		var utkozoSz = _boxCollider.size.x * Mathf.Abs(transform.localScale.x) - (2 * skinWidth);			//tulajdonképp az ütköző szélességének kiszámítása venni kell a box collider szélességét és szorozni 
		rayVTav=utkozoSz/(osszFRay-1);																		//kell a localscale-el(azért mert lehet az objektum méretét változtatjuk a méretét)
																											//(abszolút érték mer akár meg is fordulhat a karakter és akkor -1x lesz a localscale.x)
																											//utána a skinwidth-t kikell vonni 2x mer ugye a két szélén van akarakternek

		var utkozoM = _boxCollider.size.y * Mathf.Abs(transform.localScale.y) - (2 * skinWidth);			//mint az előbb csak pepitába	
		rayFTav = utkozoM / (osszVRay - 1);

	}



	//az a metódus ami legutáljára hívódik meg automatikusan
	public void LateUpdate()
	{

		ugriJarJar -= Time.deltaTime;							//ezt csökkenteni kell és akkor ha 0 akkor ugorhat megint
		_sebesseg.y += parameterek.gravitacio * Time.deltaTime;
		Mozgas (sebesseg * Time.deltaTime);	//deltaTime mennyi idő telt el az előző frame óta(mp)(nagyon kicsit szám(<1))

	}

	public void PluszEro(Vector2 ero)
	{

		_sebesseg = ero;

	}

	public void SetEro(Vector2 ero)
	{

		_sebesseg += ero;
	}

	public void SetVEro(float x)
	{
		_sebesseg.x = x;
	}

	public void SetFero(float y)
	{
		_sebesseg.y = y;
	}

	public void Ugras()
	{
		

		PluszEro (new Vector2 (0, parameterek.ugrasEreje));//hozzáadjuk a függőleges részhez az erőt amivel így ugorhat
		ugriJarJar = parameterek.ugrasGyakorisag;//megkapja az alap paramétert

	}


	private void Mozgas(Vector2 deltaMozgas)
	{

		var foldonVolt = allas.UtkozikLent;
		allas.Reset ();

		if (utkozesKezeles) 
		{
		
			PlatformKezeles ();//előszőr a platformot kezeljük
			KezdoRay (); // a rayek kezdőpontják kiszámoljuk

            
			if (Mathf.Abs (deltaMozgas.x) > 0.001f)//amennyiben ez kisebb mint ez az érték az azt jelenti, hogy egy helyben állunk és nem kell a rayeket kilőni
				MozgasV (ref deltaMozgas); //ref mer változnia kell

			
		
			MozgasF(ref deltaMozgas);//ez mindenképp lefut, mert pl a gravitáció is hat a függőleges mozgásra

			vizszintesKezeles (ref deltaMozgas, true);	//jobbra lövünk
			vizszintesKezeles (ref deltaMozgas, false);	//aztán balra



		}

		_transform.Translate (deltaMozgas, Space.World); //itt történik a mozgatás tulajdonképp


		if (Time.deltaTime > 0)
			_sebesseg = deltaMozgas / Time.deltaTime;


        //kisebbet adja majd vissza a sebességünknek így sose mehetünk túl a maximum sebességen
        _sebesseg.x = Mathf.Min(_sebesseg.x, parameterek.maxGyorsasag.x); 
		_sebesseg.y = Mathf.Min (_sebesseg.y, parameterek.maxGyorsasag.y);
        

		//azért kerül ezek utána ez a kód mer az alapvető sebesség modosítások befejeződtek és nem tudnak beleszolni
		if (allokVmin != null)
		{
			globalPontPl = transform.position;
			helyPontPl = allokVmin.transform.InverseTransformPoint (transform.position);

            Debug.DrawLine(transform.position, globalPontPl);
            Debug.DrawLine(transform.position, helyPontPl);

            if (utolsoAlldogalas != allokVmin) {
			//üzenet küldések amik arra jók, hogy különböző classokban fellehessen őket hasznánli 
				if (utolsoAlldogalas != null)
					utolsoAlldogalas.SendMessage ("Kontroller2DExit", this, SendMessageOptions.DontRequireReceiver);	


				allokVmin.SendMessage ("KontrollerEnter2D", this, SendMessageOptions.DontRequireReceiver);//UgroPlatform használja ezt
				utolsoAlldogalas = allokVmin;

			} else if (allokVmin != null)
				allokVmin.SendMessage ("KontrollerStay2D", this, SendMessageOptions.DontRequireReceiver);
			
		}
		else if (utolsoAlldogalas != null)
		{

			utolsoAlldogalas.SendMessage ("KontrollerExit2D", this, SendMessageOptions.DontRequireReceiver);
			utolsoAlldogalas = null;

		}			

	}

	private void PlatformKezeles()
	{
		//minden objektumnak kiszámolja a sebességét amin állok
		if (allokVmin != null) {
		    
            
			var ujPlatformPont = allokVmin.transform.TransformPoint (helyPontPl);//kiszámoljuk a platform helyét
			var tav = ujPlatformPont - globalPontPl;
		
			if (tav != Vector3.zero)
				transform.Translate (tav, Space.World);		//ezzel "ragasztom rá a platformra a karaktert"

			platformSebesseg = (ujPlatformPont - globalPontPl) / Time.deltaTime;		

		} else
			platformSebesseg = Vector3.zero;

		allokVmin = null;

	}

	private void vizszintesKezeles(ref Vector2 deltaMozgas, bool jobbraLo)
	{
	    //Ez a metódus arra van, hogy a mozgó platform ne tudjon a karakterünk belsejébe jutni
		var szfel = (_boxCollider.size.x * _localScale.x) / 2;					//kell nekünk a boxCollidernek a fele viszont a localscale is kell a scaleing miatt
		var rayOrigin = jobbraLo ? _raycastJobbLent : _raycastBalLent;			//itt eldöntöm merre is lövök

		if (jobbraLo)
			rayOrigin.x -= (szfel - skinWidth); // a rayeket most középről fogjuk kilőni nem pedig a széléről
		else 
			rayOrigin.x += (szfel - skinWidth);

		var rayIrany = jobbraLo ? Vector2.right : Vector2.left;
		var offset = 0f;

		for (var i = 1; i < osszVRay - 1; i++) 
		{

			var rayVector = new Vector2 (deltaMozgas.x + rayOrigin.x, deltaMozgas.y + rayOrigin.y + (i * rayFTav)); //kilőjük a rayeket
			Debug.DrawRay(rayVector,rayIrany *szfel,jobbraLo ? Color.cyan : Color.magenta);
		
			var raycastHit = Physics2D.Raycast (rayVector, rayIrany, szfel, platfomMaszk);

			if (!raycastHit)
				continue;

			//ezzel állapítom meg hogy merre kell visszarugnia
			offset = jobbraLo ? ((raycastHit.point.x - _transform.position.x) - szfel) : (szfel - (_transform.position.x - raycastHit.point.x));
		
		}

		
		deltaMozgas.x += offset;
	
	}


	//honnan indulnak a rayek
	private void KezdoRay()
	{
        //A rayeknek azért van fontos szerepe, mert meghatározzák, hogy a karakter pontosan melyik részén fog ütközni(teteje,közepe,alja) és ezáltal megtud akadni

        //ezek jelentik a rayek(sugarak) kezdőpontjainak meghatározását(balfen,jobblent,ballent) 
		var meret = new Vector2 (_boxCollider.size.x * Mathf.Abs (_localScale.x), _boxCollider.size.y * Mathf.Abs (_localScale.y))/2;
        //tilajdonképp a egy bozcollidert fogunk kiszámolni(vesszük aza alapboxcollidert és felszorozzuk a localscale-el(ha változik ez az érték akkor is jó lesz így))
        //valamint osztjuk 2-vel így szépen a karakterhez "simul"
		var center = new Vector2 (_boxCollider.offset.x * _localScale.x, _boxCollider.offset.y * _localScale.y);
        //kiszámoljuk a középpontot(localscale ugyanezen ok miatt kell)

        //kiszámoljuk a kezdőpontokat a rayeknek
		_raycastBalFent = _transform.position + new Vector3(center.x - meret.x + skinWidth,center.y + meret.y - skinWidth);
		_raycastJobbLent = _transform.position + new Vector3 (center.x + meret.x - skinWidth, center.y - meret.y + skinWidth);
		_raycastBalLent = _transform.position + new Vector3 (center.x - meret.x + skinWidth, center.y - meret.y + skinWidth);

	}

	private void MozgasV(ref Vector2 deltaMozgas)
	{

		var jobbraMegy = deltaMozgas.x > 0;									//ha az deltaMozgas.x nagyobb mint 0 akkor jobbramegyünk
		var rayTav = Mathf.Abs (deltaMozgas.x) + skinWidth;					//itt állítódik be a ray-nek a hossza

		var rayIrany = jobbraMegy ? Vector2.right : Vector2.left;			//itt pedig az iránya

		var rayOrigin = jobbraMegy ? _raycastJobbLent : _raycastBalLent;	//ha jobbra megy akkor a ray-eket jobb alsó kezdő rayt kell használni egyébként bal alsót

		//ebben ciklusban lesznek kilőve a ray-ek
		for (var i = 0; i < osszVRay; i++) 
		{
		
			var rayVector = new Vector2 (rayOrigin.x, rayOrigin.y + (i * rayFTav));	//kilőjük egyenként a sugarakat ahogy haladunk az i növekszik és a rayek(sugarak) y értéke nő(fentebbről lövi ki )
			Debug.DrawRay(rayVector,rayIrany * rayTav,Color.red);					//debug scene view-ba meglehet nézni a rayeket

			var rayCastHit = Physics2D.Raycast (rayVector, rayIrany, rayTav, platfomMaszk); //találatos paraméterek(honnan,merre,milyen messze,maszk(amivel ütközhet a karakter))


            //ha nem talál akkor folytatás(ami ez alatt van nem hajtódik végre)
            if (!rayCastHit)
				continue;
			
			//ez akkor fog lefutni ha találat van
			deltaMozgas.x = rayCastHit.point.x - rayVector.x; //kiszámoljuk, hogy mennyit tudunk menni az akadályig

			rayTav = Mathf.Abs (deltaMozgas.x);

			//itt az ütköztetések
			if (jobbraMegy)
			{

				deltaMozgas.x -= skinWidth;//ezt lekel vonni és nem lesz hézag a karakter és azütköztetett objektum között
				allas.UtkozikJobb = true;

			} else 
			{
				deltaMozgas.x += skinWidth;//itt azért hozzáadom mert tulajdonképp negatív számról van szó 
				allas.UtkozikBal = true;

			}

			if (rayTav < skinWidth + .0001f)
				break;
				
			

		}
	}
	//ez kb a MozgasV csak y-ra
	private void MozgasF(ref Vector2 deltaMozgas)
	{

		var felfeleMegy = deltaMozgas.y > 0;
		var rayTav = Mathf.Abs (deltaMozgas.y) + skinWidth;
		var rayIrany = felfeleMegy ? Vector2.up : Vector2.down;
		var rayOrigin = felfeleMegy ? _raycastBalFent : _raycastBalLent;

		rayOrigin.x += deltaMozgas.x;

		var alloAllapot = float.MaxValue;

		for (var i = 0; i < osszFRay; i++) 
		{

			var rayVector = new Vector2 (rayOrigin.x + (i * rayVTav),rayOrigin.y);
			Debug.DrawRay (rayVector, rayIrany * rayTav, Color.red);

			var raycastHit = Physics2D.Raycast (rayVector, rayIrany, rayTav, platfomMaszk);

			if (!raycastHit)
				continue;

			//ilyenkor esünk/lefele megyünk
			if (!felfeleMegy) 
			{
			
				var foldtolValoTav = _transform.position.y - raycastHit.point.y;

				if (foldtolValoTav < alloAllapot) 
				{
				
					alloAllapot = foldtolValoTav;
					allokVmin = raycastHit.collider.gameObject;

				}

			}
		
			deltaMozgas.y = raycastHit.point.y - rayVector.y;
			rayTav = Mathf.Abs (deltaMozgas.y);

			if (felfeleMegy) {
			
				deltaMozgas.y -= skinWidth;
				allas.UtkozikFent = true;

			} else 
			{
			
				deltaMozgas.y += skinWidth;
				allas.UtkozikLent = true;

			
			}

			if (rayTav < skinWidth + .0001f) 
				break;

		}

	}

	
}


