﻿using UnityEngine;

public class LebegoText : MonoBehaviour
{

    private static readonly GUISkin skin = Resources.Load<GUISkin>("GameSkin");  //ez fogja betölteni a gui skint ami majd kelleni fog a textekhez
    private GUIContent _content;//ez a tartalom
    private ILebegoTextPozicio _pozicio;    //ez majd vagy VilagiTextPozició vagy a TextPozicioKozep

    //public string text { get { return _content.text; } set { _content.text = value; } }
    public GUIStyle stilus { get; set; }


    public static LebegoText Mutat(string textem, string stilusom, ILebegoTextPozicio poziciom)
    {
        //amit kell azt beállítunk a paraméterekből
        var gameO = new GameObject("Lebego Text");
        var lebegoText = gameO.AddComponent<LebegoText>();
        lebegoText.stilus = skin.GetStyle(stilusom);
        lebegoText._pozicio = poziciom;
        lebegoText._content = new GUIContent(textem);
        return lebegoText;
    }

    
    
    public void OnGUI()
    {
        //ez végzi a megjelenítést
        var pozicio = new Vector2();
        var contentMeret = stilus.CalcSize(_content);
        //ha nem igaz akkor nem kell semmit se csinálni tulajdonképp
        if (!_pozicio.GetPozicio(ref pozicio, _content, contentMeret))
        {

            Destroy(gameObject);
            return;

        }
        //létrehozzuk a feliratot tulajdonképp, ami 1 label lesz a rect-el meghatározzuk a poziciót és egyebeket,utána kell meg amit beleírunk utána pedig a stílus
        GUI.Label(new Rect(pozicio.x, pozicio.y, contentMeret.x, contentMeret.y), _content, stilus);
    }


}

