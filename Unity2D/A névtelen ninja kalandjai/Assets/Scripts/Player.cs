﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour , ISebzes
{

	private bool jobbraNez;						//jobbra néz-e a karakter(haladási irány balról jobbra)
	private KarakterKontroller2D kontroll;		//ebből dolgok majd meglesznek hívva amik kellenek
	private float nMozgasSebesseg;				// ennek az értéke 1 vagy -1 lesz annak függvényébe hogy balra vagy jobbra mozog
    private float lohet;                        //ha ez 0 akkor tud újra lőni a player

	public float maxSebesseg = 8f;				//max sebesség
	public float gyorsulasFold = 10f;			//gyorsulás földön		
	public float gyorsulasLevego = 5f;		   	//gyorsulás levegőben
    public int maxHP = 100;                     //a player max hp-ja
    public int maxStamina = 100;                //max Stamina
    public GameObject ezFajtEffect;             //effekt, ha eltalálnak
    public Lovedek[] lovedekem;                 //a palyernek a lövedéke
    public float tuzGyorsasag;                  //ilyen gyorsan tüzelhet a karakter
    public Transform kilovoPont;                //innen lövi ki a lövedékeket
    public Animator animatorom;                 //animáláshoz                    
    public AudioClip dobasHang;                 //a dobásnak hangja

    public int stamina { get; private set; }    //aktuális stamina     
    public int HP { get; private set; }         //a palyer aktuális hp-ja
    public bool halott { get; private set;}     //lecheckoljuk hogy halott-e az illető

	public void Awake()
	{
		
		kontroll = GetComponent<KarakterKontroller2D> ();   //lekérjük a komponenseket hozzá
		jobbraNez = transform.localScale.x > 0;             //ha x tengelyen az x nagyobb mint 0 akkor jobbranézünk
        HP = maxHP;                                         //az aktuális hp-nak megadjuk első körben a maxHP-t  
        stamina = maxStamina;                               //az aktuális stamina-nak-nak megadjuk első körben a maxStamina-t-t  

    }

	public void Update()
	{

        lohet -=Time.deltaTime;

		//csak akkor kezelje az inputot ha él
		if(!halott)
			InputKezelo ();

		var mozgasFaktor = kontroll.allas.Foldon ? gyorsulasFold : gyorsulasLevego; //a mozgásnak a stílusát(sebességét)állítjuk be

        //ne tudjon mozogni
		if (halott)
			kontroll.SetVEro (0);
		else //vízszintes mozgás
			kontroll.SetVEro (Mathf.Lerp (kontroll.sebesseg.x, nMozgasSebesseg * maxSebesseg, Time.deltaTime * mozgasFaktor));
        
        //animációk bizonyos esetekre
        animatorom.SetBool("Foldon",kontroll.allas.Foldon);
        animatorom.SetBool("Halott", halott);
        animatorom.SetFloat("Sebesseg", Mathf.Abs(kontroll.sebesseg.x) / maxSebesseg);
        
	}
    //nem mozog,nem szuszog, nem lélegzik 
    public void SzintVege()
    {
        
        enabled = false;
        kontroll.enabled = false;
        animatorom.SetFloat("Sebesseg", 0);


    }


    public void Gyilkos()
	{

		kontroll.utkozesKezeles = false;	        //halál esetén nem kezeli az ütközést
		GetComponent<Collider2D>().enabled = false;	//ez is kikapcsolva
		halott = true;								//beállítjuk ezt is
        HP = 0;                                     //halott tehát nincs neki HP-ja
        kontroll.SetEro(new Vector2(0, 15));        //kicsit feldob
		
	}

	//feléledéshez
	public void ReSpawn(Transform spawnPoint)
	{
        
		//ha a karakter nem jobbra néz akkor átfordítja
		if (!jobbraNez)
			Flip ();
		
		
		//tulajdonképp visszaállítunk mindent amit a Gyilkos()ba átállítottunk
		halott = false;
		GetComponent<Collider2D> ().enabled = true;
		kontroll.utkozesKezeles = true;
        HP = maxHP;                                 //visszaállítjuk a HP-ját
		transform.position = spawnPoint.position;	//beállítjuk a spawnPointhoz

	}

    public void AdHp(int hpm, int staminam, GameObject elkoveto)
    {

        StartCoroutine(AdHpCo(hpm, staminam, elkoveto));

    }
    // a coroutine-ra azért van szükség mert itt lehet yield return alkalmazni ami nem rendes return csak 1 beakasztja a programot pl fél mp-ig
    public IEnumerator AdHpCo(int hpm, int staminam, GameObject elkoveto)
    {
        //ha felvesszük a hp dobozt akkor kiírja, hogy mennyi hp-t illetve staminat kapunk(ahol a karakter is áll) 
        LebegoText.Mutat(string.Format("+{0}!", hpm), "PluszHpText", new VilagiTextPozicio(Camera.main, transform.position, 1.5f, 50));
        yield return new WaitForSeconds(0.5f);
        LebegoText.Mutat(string.Format("+{0}!", staminam), "PluszHpText", new VilagiTextPozicio(Camera.main, transform.position, 1.5f, 50));
        //gozzáadjuk a plusz stamina-t illetve hp-t, de maximum csak annyit amennyit lehet(100 maxhp esetén nem megyünk felépl 110-re)
        stamina = Mathf.Min(stamina + staminam, maxStamina);
        HP = Mathf.Min(HP + hpm, maxHP);

    }

    //sebzéshez
    public void Sebzes(int dmg, GameObject elkoveto)
    {
        //kiírja a sebzés mértékét ott ahol maga a karakter is van
        LebegoText.Mutat(string.Format("-{0}!", dmg), "PlayerSebzodikText", new VilagiTextPozicio(Camera.main, transform.position, 2f, 60f));
        //itt behúzzuk az effectet a playerre
        Instantiate(ezFajtEffect, transform.position, transform.rotation);  
        HP -= dmg;  //Csökkentjük a HP-t

        //ha a HP <=0 akkor megöljök a playert egyszerűen
        if (HP <= 0)
            LevelManager.peldany.PlayerGyilkos();
    }


	//privát metódusok
	private void InputKezelo()
	{

		//ha megnyomjuk a D betüt akkor a normalizált sebesség 1 lesz vagyis jobb felé kell majd menni amenyiben a járékos nem jobbranéz akkor megforgatja
		if (Input.GetKey (KeyCode.D)) {

			nMozgasSebesseg = 1;

			if (!jobbraNez)
				Flip ();

		}//ugyanaz mint az előbb csak pepitába
		else if (Input.GetKey (KeyCode.A)) {

			nMozgasSebesseg = -1;

			if (jobbraNez)
				Flip ();

		} else
			nMozgasSebesseg = 0;	//ilyenkor állunk 1 helyben

		//amennyiben a karakter tud ugrani és megnyomjuk az W betüt akkor ugrik

		if (kontroll.ugorhat && Input.GetKeyDown(KeyCode.W)) 
		{
            			
			kontroll.Ugras();
            			
		}

        if (Input.GetKeyDown(KeyCode.L))
        {
           
            Tuzeles(0);

        }

        if (Input.GetKeyDown(KeyCode.K) && stamina>=10 && lohet < 0)
        {            

            stamina -= 10;
            Tuzeles(1);

        }
        if (Input.GetKeyDown(KeyCode.J) && stamina >= 20 && lohet < 0)
        {
           
            stamina -= 20;
            Tuzeles(2);

        }
        

    }

    private void Tuzeles(int index)
    {

        

        //mivel nagyobb mint 0 nem löhet a player
        if (lohet > 0)
            return;
        //a dobás hangját lejátszuk
        if (dobasHang != null)
            AudioSource.PlayClipAtPoint(dobasHang, transform.position);

        var irany = jobbraNez ? Vector2.right : Vector2.left;   //ha jobbranéz jobbra lő ha balra akkor balra

        //létrehozzuk a lövedéket az index alapján(hogy melyiket lőjök ki)
        var _lovedek = (Lovedek)Instantiate(lovedekem[index], kilovoPont.position, kilovoPont.rotation);
        //tulajdonos,irány,sebesség
        _lovedek.Init(gameObject, irany, kontroll.sebesseg);
        //ezt visszaállítjuk
        lohet = tuzGyorsasag;
        //animációt lejátszuk
        animatorom.SetTrigger("Tuz");

    }

    //karakter megfordítós script
    private void Flip()
	{
	
		transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
		jobbraNez = transform.localScale.x > 0;

	}
}
