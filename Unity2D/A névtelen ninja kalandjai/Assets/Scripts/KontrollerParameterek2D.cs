﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class KontrollerParameterek2D 
{
	//ugrás típusok
	public enum UgrasViselkedes
	{
		UgrasFold,
		UgrasBarhol,
		UgrasN
	}

	public Vector2 maxGyorsasag = new Vector2(float.MaxValue, float.MaxValue);
	public float gravitacio = -25f;												//gravitáció mérete
	public UgrasViselkedes ugrasKorlat;                 
	public float ugrasGyakorisag = .25f;
	public float ugrasEreje = 12;

}

