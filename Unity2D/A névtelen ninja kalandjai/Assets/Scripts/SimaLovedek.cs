﻿using System;
using UnityEngine;


public class SimaLovedek : Lovedek, ISebzes
{

    public int dmg;                     //a lövedék sebzése
    public GameObject robbanasEffect;   //robbanás effect  
    public float eltav;                 //mennyi ideig van "életben" a lövedék


    public void Update()
    {

        //ha letelik az eltáv megsemmisül a lövedék
        if ((eltav -= Time.deltaTime)<=0)
        {
            lovedekMegsemmisit();
            return;
        }

        //sima lövedékünknek megadjuk az irányát,a sebességét és hogy hol mozogjon
        transform.Translate(irany * sebesseg * Time.deltaTime, Space.World); 
    }
    //megsemmisül bármilyen sebzésre
    public void Sebzes(int dmg, GameObject elkoveto)
    {

        lovedekMegsemmisit();

    }

    //ha nem playerrel ütközünk akkor csak simán megsemmisítjük a lövedéket
    protected override void UtkozEgyeb(Collider2D collide)
    {

        lovedekMegsemmisit();

    }

    //hogyha ütközik a lövedék a playerrel akkor segződik a player
    protected override void UtkozSebez(Collider2D collide, ISebzes sebzes)
    {
        //itt tulajdonképp fellépünk az interface-be
        sebzes.Sebzes(dmg, gameObject);

        lovedekMegsemmisit();
    }

    

    private void lovedekMegsemmisit()
    {
        //amennyiben a lovedének van effectje végrehajtjuk(effect,lövedék pozició,hogy van elforgatva)
        if (robbanasEffect != null)
            Instantiate(robbanasEffect, transform.position, transform.rotation);
        
        //megsemmisítjük a gameobjectet ezzel memóriát szabadítunk fel
        Destroy(gameObject);

    } 
}

