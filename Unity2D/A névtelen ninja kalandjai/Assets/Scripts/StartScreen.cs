﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScreen : MonoBehaviour
{

    public string elsoSzint;

    public void Update()
    {

        if (!Input.GetMouseButton(0))
            return;

        GameManager.peldany.Reset();

        SceneManager.LoadScene(elsoSzint);   //kattintásra betöltjük az első szintet

    }

}

