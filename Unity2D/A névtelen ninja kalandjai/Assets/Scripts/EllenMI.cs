﻿using System.Collections.Generic;
using UnityEngine;


public class EllenMI:MonoBehaviour , ISebzes, IPlayerRespawnListener
{
    private bool jobbraNez;
    private KarakterKontroller2D kontroller;
    private Vector2 irany;
    private Vector2 kezdoPont;
    private Vector2 _sebesseg;
    private float lohet;

    public int hpMax;
    public float sebesseg;
    public float tuzGyorsasag;
    public int sebzes;
    public Lovedek[] lovedek;
    public GameObject robbanasEffect;
    public AudioClip dobasHang;
    public Animator animatorom;

    public bool halott { get; private set; }
    public int HP { get; private set; }

    public void Awake()
    {
        HP = hpMax;
        jobbraNez = transform.localScale.x > 0;
        
    }

    public void Start()
    {
       
        kontroller = GetComponent<KarakterKontroller2D>();
        irany = new Vector2(-1, 0);
        kezdoPont = transform.position;
       
    }

    public void Update()
    {
       
        animatorom.SetFloat("Sebesseg", Mathf.Abs(sebesseg));

       

        
        kontroller.SetVEro(irany.x * sebesseg);
      

        //ha az enemy falnak ütközik akkor az ellenkező irányba indítja vissza
        if ((irany.x < 0 && kontroller.allas.UtkozikBal) || irany.x > 0 && kontroller.allas.UtkozikJobb)
        {

            irany = -irany;
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.y); 

        }

        //amíg ez nagyobb mint 0 nem lőhet
        if ((lohet -= Time.deltaTime) > 0)
            return;

       

        var raycast = Physics2D.Raycast(transform.position, irany, 10, 1 << LayerMask.NameToLayer("Player"));
       
        if (!raycast)
            return;

        //dobásnak a hangját lejátszuk
        if (dobasHang != null)     
            AudioSource.PlayClipAtPoint(dobasHang, transform.position);
            
        


        //a tömben valamelyik elemet kiválasztjuk(azt fogja dobni)
        int indexem = Random.Range(0, 2);
        
        
        var _lovedek = (Lovedek)Instantiate(lovedek[indexem], transform.position, transform.rotation);
        

        _lovedek.Init(gameObject, irany, kontroller.sebesseg);
        lohet = tuzGyorsasag;

       
        

    }

    public void OnTriggerEnter2D(Collider2D utkoz)
    {

        var player = utkoz.GetComponent<Player>();  //lekérjük a Player komponenseket

        if (player == null)                         //ha ütközünk, de nem playerrel akkor return
            return;

        animatorom.SetTrigger("Ut");
        player.Sebzes(sebzes, gameObject);

        var kontroll = player.GetComponent<KarakterKontroller2D>();
        var totalSebesseg = kontroll.sebesseg + _sebesseg;

        kontroll.SetEro(new Vector2(
            -1 * Mathf.Sign(totalSebesseg.x) * Mathf.Clamp(Mathf.Abs(totalSebesseg.x) * 6, 10, 40),     //visszalökés iránya, visszalökés ereje
            -1 * Mathf.Sign(totalSebesseg.y) * Mathf.Clamp(Mathf.Abs(totalSebesseg.y) * 6, 0, 25)));



    }


    public void Sebzes(int dmg, GameObject elkoveto)
    {
        HP -= dmg;
        if (HP <= 0)
        {
            
           
            Instantiate(robbanasEffect, transform.position, transform.rotation);
            gameObject.SetActive(false);

        }
            
    }
    //halál után visszaállítás
    public void OnPlayerRespawnCheckpoint(CheckPoint cPont, Player player)
    {
        HP = hpMax;
        irany = new Vector2(-1, 0);
        transform.localScale = new Vector3(1, 1, 1);
        transform.position = kezdoPont;
        halott = false;
        gameObject.SetActive(true);
    }
}

