﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public class Utmeghatarozo : MonoBehaviour
{

	public Transform[] pontok;  //ebbe a tömbe kerülnek a pontok azért transform mer így különböző transzformációkat lehet rajtuk végrehajtani(pl pozició változtatás,forgatás stb)

	//ezzel lesz majd ping-pong effektus
	public IEnumerator<Transform> KapottUtvonal()
	{

		if (pontok == null || pontok.Length < 1)		//ha kisebb a pontok tömböm elemszáma mint 1 akkor return(mert ha csak 2> pontom van a tömben nincs mivel összekötni) és még null esetén is
			yield break;

		var irany = 1;
		var index = 0;

		//ez a rész fogja biztosítani azt a biznyos ping-pong effektust(ez 1 végtelen ciklus)
		while (true) {

			yield return pontok [index];			//ez visszaadja a pontok[indexedik] elemét, de utána tovább megy

			if (pontok.Length == 1)					//ha a pontok tömbnek csak 1 eleme van akkor innentől kiugrunk a ciklusból
				continue;

			if (index <= 0)							//ha  0 vagy kisebb az index akkor az 1-es irány
				irany = 1;
			else if (index >= pontok.Length - 1)	//ha  elértük a tömb végét a -1-es irány(megfordul)
				irany = -1;


			index += irany;	//növelem az indexet

		}
	
	}

	//hasonlóan működik mint a sima gui elemeknél a megjelenítés kivéve,hogy csak a scene view-ban látszódnak a kirajzolások(debug miatt is jó)
	public void OnDrawGizmos()
	{
	
		if (pontok == null || pontok.Length < 2)		//ha kisebb a pontok tömböm elemszáma mint 2 akkor return(mert ha csak 2> pontom van a tömben nincs mivel összekötni) és még null esetén is
			return;

		for (int i = 1; i < pontok.Length ; i++) {

			Gizmos.DrawLine (pontok [i-1].position, pontok [i].position);	//két pontot összeköt a drawline metódus(ugyebár ez csak a scene viewban lesz látható)

		}

	}
}
