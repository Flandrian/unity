﻿using UnityEngine;
using System.Collections;
/*Ez a class menedzseli a pontokat és hasonlókat 
     */
public class GameManager
{
	private static GameManager _peldany;
	public static GameManager peldany { get { return _peldany ?? (_peldany = new GameManager());}}
	public int pontjaim { get; private set;}

	private GameManager()
	{
	}

	public void Reset()
	{
		pontjaim = 0;
	}

	public void ResetPontok(int pontok)
	{
	
		pontjaim = pontok;

	}

	public void PontotAd(int pontok)
	{
		pontjaim += pontok;
	}

}

