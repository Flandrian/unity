﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
   
    private Player player;  //player információkat fog tartalmazni
    public GameObject pause;    //ez pedig azt a canvas objektumot amin a form/UI van

    public void Awake()
    {
        Menu.pause = null;
        player = FindObjectOfType<Player>();    //megkeressük az ilyen típusú obejtumokat az adott pályán
    }

    public void Update()
    {
        //Escape gomb lenyomása esetén történnek dolgok
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //ha a pause objektumunk a hierarhiában false akkor töténnek dolgok
            if (pause.gameObject.activeInHierarchy == false)
            {
                
                pause.gameObject.SetActive(true);   //aktívvá tesszük a a pause gameobjektumot
                Time.timeScale = 0;                 //megállítjuk az időt ezáltal nem mozog semmi a pályán
                AudioListener.pause = true;         //a zenét is megállítjuk
                player.enabled = false;             //player mozgását is megállítjuk, hogy ne legyen mozgást
            }
            else //visszaállítunk mindent(Escape újboli lenyomása után)
            {
                
                pause.gameObject.SetActive(false);
                AudioListener.pause = false;
                Time.timeScale = 1;
                player.enabled = true;


            }

        }

    }
   
    public void VisszaTerkep()
    {
        Menu.pause = true;  //ez azt jelzi majd a programnak, hogy a pause menüböl megyünk a térképhez
        SceneManager.LoadScene("Menu");
    }

    public void VisszaFomenu()
    {
        Menu.pause = false;
        SceneManager.LoadScene("Menu");

    }



    public void Kilepes()
    {

        Application.Quit();

    }

   
}

