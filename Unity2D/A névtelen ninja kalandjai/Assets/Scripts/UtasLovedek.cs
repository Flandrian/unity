﻿using System;
using UnityEngine;

public class UtasLovedek : MonoBehaviour,ISebzes
{ 
    public GameObject robbanasEffect;
    private Transform _cel;
    private float _sebesseg;
    

    public void Init(Transform cel, float sebesseg)
    {

        _cel = cel;
        _sebesseg = sebesseg;

    }


    public void Update()
    {

        transform.position = Vector3.MoveTowards(transform.position, _cel.position, Time.deltaTime * _sebesseg);    //kezdő,cél,sebesség

        var tav = (_cel.transform.position - transform.position).sqrMagnitude;                                      //négyzetes hossza a vector3-nak

        //ha a táv nagyobb mint a reláció jobb oldala akkor nincs mit csinálni
        if (tav > .01f * .01f)
            return;

        if (robbanasEffect != null)
            Instantiate(robbanasEffect, transform.position, transform.rotation);

        
        Destroy(gameObject);
        
    }
    //bármilyen sebzés éri megsemmisül
    public void Sebzes(int dmg, GameObject elkoveto)
    {

        if (!robbanasEffect != null)
            Instantiate(robbanasEffect, transform.position, transform.rotation);

        Destroy(gameObject);
    }   

}

