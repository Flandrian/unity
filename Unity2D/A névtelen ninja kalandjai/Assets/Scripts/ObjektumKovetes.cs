﻿using UnityEngine;


public class ObjektumKovetes : MonoBehaviour
{

    public Vector3 offset;  //ezzel beállíthatom a helyezkedését
    public Transform kovetes;//ezzel pedig rátudom ragasztani valamire(pl player objektum) 
    //Ez végzi a követést
    public void Update()
    {
        //lekérjük az objektumunk pozicióját és hozzáadjuk az offsetet(ha nem tennénk akkor az objektumban jelenne meg(fedés lenne,de ha pl y tengelyen 2.5 állítunk be akkor szépen fölötte lesz))
        transform.position = kovetes.transform.position + offset;

    }
}

