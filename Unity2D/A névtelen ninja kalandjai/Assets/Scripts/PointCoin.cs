﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class PointCoin : MonoBehaviour, IPlayerRespawnListener
{

    public GameObject effect;       //az effect mikor felvesszük a coin-t
    public int pontAd = 5;          //ennyi pontot adunk hozzá a pontjainkoz
    public AudioClip coinHang;

    //Ütközés esetén megtörténik a coin felvétele a pontok hozzáadása
    public void OnTriggerEnter2D(Collider2D vmi)
    {
        //ha nem a player ütközik a coinnal akkor nem kell csinálni semmit se
        if (vmi.GetComponent<Player>() == null)
            return;

        if (coinHang != null)
            AudioSource.PlayClipAtPoint(coinHang, transform.position);


        GameManager.peldany.PontotAd(pontAd);
        Instantiate(effect, transform.position, transform.rotation);  // ez a rész tulajdonképp az effectet fogja oda rakni ahol a coin van

        //kiírja mennyi pontot kaptunk, beállítja melyik kiírási módot válassza(GUISKIN), beállítja hol jelentjen meg a kiírás (main kamerán,a coinnak a poziciója,mennyi idő alatt tünjön el, milyen gyorsan menjen felfele
        LebegoText.Mutat(string.Format("+{0}!", pontAd), "PontCoinText", new VilagiTextPozicio(Camera.main, transform.position, 1.5f, 50f));


        gameObject.SetActive(false);  //az adott coint kivesszük a számításból de csak így mert később ha meghalunk checkpoint előtt akkor visszakell aktiválni


    }
    //visszakerülnek a dolgok
    public void OnPlayerRespawnCheckpoint(CheckPoint cPont, Player player)
    {
        gameObject.SetActive(true);
    }

}

