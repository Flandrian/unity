﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class Betolteshez
{
    public string nev;
    public string palyaNev;
    public string teruletNev;
    public int pontszam;

    public Betolteshez(string _nev,string _palyaNev)
    {
        nev = _nev;
        palyaNev = _palyaNev;
    }

    public Betolteshez(string _nev, string _teruletNev, string _palyaNev,int _pontszam)
    {
        nev = _nev;
        teruletNev = _teruletNev;
        palyaNev = _palyaNev;
        pontszam = _pontszam;
    }

}

