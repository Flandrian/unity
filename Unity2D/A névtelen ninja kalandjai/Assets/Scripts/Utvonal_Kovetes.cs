﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Utvonal_Kovetes : MonoBehaviour
{
	//hogyan kovesse az utvonalat
	public enum kovetestipus
	{
		
		PontFele

	}

	public kovetestipus tipus = kovetestipus.PontFele;						// követés tipusának default értéke
	public Utmeghatarozo utvonal;											// maga az utvonal
	public float sebesseg = 1;												// maga az objektum sebessége
	public float maxtavCel = .1f;											// maximum táv amikor az adott pontról már továbbléphet 	

	private IEnumerator<Transform> aktualisPont;

	
	public void Start()
	{
		//rögtön hibakezelés, ha a
		if (utvonal == null) 
		{
		
			Debug.LogError ("Az utvonal nem lehet null", gameObject);
			return;
		
		}


		aktualisPont = utvonal.KapottUtvonal ();							//lekérjük az aktuális pontot

		aktualisPont.MoveNext ();											//egyből léptetek is
        

		//ha ez van akkor nem megyünk tovább
		if (aktualisPont.Current == null)									
			return;

		transform.position = aktualisPont.Current.position;     //a platform poziciója az aktuális pontra megy át
	
	}

	public void Update()
	{
		//ha maga az aktuális pont null vagy a benne található pont null akkor return
		if (aktualisPont == null || aktualisPont.Current == null) 			
			return;
			
		//ha a követéstips az hogy a pont felé megyünk akkor a Vector3-nak egy metódusát használva fog mozogni a platform
		if (tipus == kovetestipus.PontFele) 
		{
            //csak,hogy értsük mit csinál a metódus(alap pozició,cél pozício,maxtavolsag)
			transform.position = Vector3.MoveTowards (transform.position, aktualisPont.Current.position, Time.deltaTime * sebesseg);
            //a végén tulajdonképp a transform.position=aktualisPont.Current.position
            
        }

        //ez 1 pici szám lesz
		var tav = (transform.position - aktualisPont.Current.position).sqrMagnitude;
        //ha ez a pici szám elég kicsi
		if (tav < maxtavCel * maxtavCel)
			aktualisPont.MoveNext ();

	
	}

}

