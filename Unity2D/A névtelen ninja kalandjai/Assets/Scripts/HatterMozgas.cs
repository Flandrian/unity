﻿using UnityEngine;
using System.Collections;

public class HatterMozgas : MonoBehaviour
{
	private Vector3 utolsoPoz;      //mivel a kamerára fogjuk rárakni ezt folyamatosan lekérdezzük
	public Transform[] hatterek;	//ezek fognak majd mozogni(layerek)
	public float mozgasSkala;		//ez fogja befolyásolni a mozgás mértékét
	public float mozgaslassítas; 	//ez azért kell mert tulajdonképp ami "messze" van tőled az lassabban mozog
	public float simitas;           //ne  darabosan mozogjon
   
    //lekérjük a kamera aktuális pozicióját és tároljuk(ez tulajdonképp inicializálás)
	public void Start()
	{
	
		utolsoPoz = transform.position;		//ez majd a kamerára vonatkozik
	
	}
    //Ez a rész mozgatja a háttereket
	public void Update()
	{

		var parallax = (utolsoPoz.x - transform.position.x) * mozgasSkala;

		for (var i = 0; i < hatterek.Length; i++)
		{
		
			var hatterPozicio = hatterek [i].position.x + parallax * (i * mozgaslassítas +1);	//a +1 azért kell hogy ha esetleg az előtte levő rész nullát adna mégiscsak mozogjon a dolog
			hatterek[i].position = Vector3.Lerp(hatterek[i].position, 							//honnan
				new Vector3(hatterPozicio,hatterek[i].position.y,hatterek[i].position.z),		//hova csúsztatja
				simitas *Time.deltaTime);														//hogy szépen csúsztassa

		}

		utolsoPoz = transform.position;															//ezt újra lekell kérni mert a Start ugye csak 1x fut le

	}

}

