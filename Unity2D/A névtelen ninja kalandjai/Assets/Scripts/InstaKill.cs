﻿using UnityEngine;
using System.Collections;

public class InstaKill : MonoBehaviour
{
    //Ütközés esetén a meghívjuk a LevelManagernek a globális változóját és megkapjuk az akivel ütközik ez a objektum
	public void OnTriggerEnter2D(Collider2D other)
	{
	    //itt a lekérés
		var player = other.GetComponent<Player> ();
        //amennyiben nem a játékosról van szó akkor nem kell csinálni semmit
		if (player == null)
			return;
        //itt pedig a globál változóra  meghívjuk a PlayerGyilkos metódust
		LevelManager.peldany.PlayerGyilkos ();

	}

}

