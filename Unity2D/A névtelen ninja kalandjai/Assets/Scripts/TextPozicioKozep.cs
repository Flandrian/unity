﻿
using System;
using UnityEngine;

public class TextPozicioKozep : ILebegoTextPozicio
{
    private readonly float _sebesseg;
    private float _textPozicio;

    public TextPozicioKozep(float sebesseg)
    {

        _sebesseg = sebesseg;

    }


    public bool GetPozicio(ref Vector2 pozicio, GUIContent content, Vector2 meret)
    {
        _textPozicio += Time.deltaTime * _sebesseg;

        if (_textPozicio > 1)
            return false;

        pozicio = new Vector2(Screen.width / 2f - meret.x / 2, Mathf.Lerp(Screen.height / 2f + meret.y, 0, _textPozicio));

        return true;

        
    }
}

