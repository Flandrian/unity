﻿using UnityEngine;
public class Ter : MonoBehaviour
{

    private Player player;
    private BoxCollider2D _boxColliderPlayer;

    public enum szelViselkedes
    {
        Semmi,
        Akadaly,
        Megol
    }
    public BoxCollider2D szelek;
    public szelViselkedes fent;
    public szelViselkedes lent;
    public szelViselkedes jobbra;
    public szelViselkedes balra;

    public void Start()
    {

        player = GetComponent<Player>();
        _boxColliderPlayer = GetComponent<BoxCollider2D>();

    }

    public void Update()
    {

        if (player.halott)
            return;
        //az ütközőméret azért kell mert ugyan lekérhetnénk csak a _boxCollidert, de akkor nem lenne pontos az ütközés és ha nő localScale attol még a boxCollider viszont 2vel is osztani kell
        var utkozoMeret = new Vector2(_boxColliderPlayer.size.x * Mathf.Abs(transform.localScale.x), _boxColliderPlayer.size.y *Mathf.Abs(transform.localScale.y))/2;
       
        if (fent != szelViselkedes.Semmi && transform.position.y + utkozoMeret.y > szelek.bounds.max.y)
            SzelekViselkedese(fent, new Vector2(transform.position.x, szelek.bounds.max.y - utkozoMeret.y));

        if(lent != szelViselkedes.Semmi && transform.position.y - utkozoMeret.y < szelek.bounds.min.y)
            SzelekViselkedese(lent, new Vector2(transform.position.x, szelek.bounds.min.y + utkozoMeret.y));

        if (jobbra != szelViselkedes.Semmi && transform.position.x + utkozoMeret.x > szelek.bounds.max.x)
            SzelekViselkedese(jobbra, new Vector2(szelek.bounds.max.x - utkozoMeret.x, transform.position.y));

        if (balra != szelViselkedes.Semmi && transform.position.x - utkozoMeret.x < szelek.bounds.min.x)
            SzelekViselkedese(balra, new Vector2(szelek.bounds.min.x + utkozoMeret.x, transform.position.y));

    }
    //hogyha nem alul éri a világ szélét akkor a módosított pozicióra dobja vissza a playert
    private void SzelekViselkedese(szelViselkedes viselkedes, Vector2 modositottPoz)
    {

        if (viselkedes == szelViselkedes.Megol)
        {

            LevelManager.peldany.PlayerGyilkos();
            return;

        }

        transform.position = modositottPoz;

    }

}

