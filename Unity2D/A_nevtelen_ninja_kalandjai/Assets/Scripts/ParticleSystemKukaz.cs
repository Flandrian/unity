﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ParticleSystemKukaz : MonoBehaviour
{

    private ParticleSystem _particleSystem;

    public void Start()
    {
        _particleSystem = GetComponent<ParticleSystem>();       //ami kell azt lekérjük
    }

    public void Update()
    {

        if (_particleSystem.isPlaying)                          //ha éppen lejátsza a dolgot akkor ne csináljon semmit
            return;

        Destroy(gameObject);                                    //ha mégse akkor kukázzuk a gameObjectet
        

    }

}

