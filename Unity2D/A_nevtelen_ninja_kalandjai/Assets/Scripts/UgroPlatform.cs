﻿using UnityEngine;
using System.Collections;

public class UgroPlatform : MonoBehaviour
{
	public float ugrasEro = 20f;
    public AudioClip ugrasHang;

	//itt állítjuk be a függőleges elmozdulás erejét
	public void KontrollerEnter2D(KarakterKontroller2D kontroller)
	{

        kontroller.SetFero(ugrasEro);

        if(ugrasHang != null)
            AudioSource.PlayClipAtPoint(ugrasHang,transform.position);

	}
}

