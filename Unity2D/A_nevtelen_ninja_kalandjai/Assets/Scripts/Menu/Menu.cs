﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Data;
using Mono.Data.Sqlite;
using System.Collections.Generic;

public class Menu : MonoBehaviour {

    private int aktivScreenResIndex;            //screenwidth[]-ből lesz kiválasztva az index
    
    private IDbConnection dbConnection;         //adatbázis kapcsolathoz kell
    private IDbCommand dbCmd;                   //itt az sql utasítás szerepel majd             
    private List<Betolteshez> neveim = new List<Betolteshez>(); //A neveket és a hozzátartozó dolgokat egy listába fogom tenni

    public static string playerName;            //játékos neve
    public static string connectionString;      //az útvonal majd az adatbázishoz
    public static string terulet;               //a pályákhoz tartozó terület
    public static bool? pause;                   //ez majd kell mikor a pausemenüből érkezünk
    public GameObject mainMenu;                 //főmenu gameobjectje
    public GameObject beallitasok;              //beállítások
    public GameObject ujJatekos;                
    public GameObject terkep;
    public GameObject SqlError;                 
    public GameObject betolto;
    public GameObject prefabButton;             //ez 1 prefab a dinamikusan létrehozott gombokhoz
    public GameObject[] Palyak;                 //adott pályákhoz 1 tömb
    public Slider hangeroSlider;                //hangerőhöz a csúszka
    public Dropdown dropDown;
    public Dropdown dropDownMinoseg;
    public float hangero;                       //hangerő
    public string palyaNev;                     //palyanév
    //Azért nincs itt érték adva a változóknak mer a public változókat a Unity Editor felismeri és a grafikusfelületen megtudjuk adni

    public void Awake()
    {
        //ez a réssz arra az esetre kell ha a pause menüből érkezünk vissza
        connectionString = "URI=file:" + Application.dataPath + "/StreamingAssets/Databases/NinjonDatabase.sqlite";
        AudioListener.pause = false;
        if (pause == false)         
            mainMenu.SetActive(true);                 
        else if (pause == true)
        {
            terkep.SetActive(true);
            OnClick();
            pause = false;

        }
        else if(pause==null)
            Beallitasok_betolto();

        hangeroSlider.value = hangero;
        Debug.Log(hangero);
        Time.timeScale = 1; //visszaállítjuk az idő múlását
        AudioListener.volume = hangero; //inicializáljuk a játék alap hangerejét
        
        //megadjuk az elérést a connectionStringnek
    }
    
   

    public void PalyakBehoz()
    {
        
        palyaNev = EventSystem.current.currentSelectedGameObject.name;
        //ezt a térképről kérjük le(az éppen aktív GameObject(ebben az esetben 1 gomb))      
        
        //kiválasztjuk a pálya típust tulajdonképp és a teruletet átírjuk az aktuális 
        switch (palyaNev)
        {
            case "Europa": Palyak[0].SetActive(true); terulet = "Europa"; break;
            case "EAmerika": Palyak[1].SetActive(true); terulet = "EAmerika"; break;
            case "Ausztralia": Palyak[2].SetActive(true); terulet = "Ausztralia"; break;
            case "Afrika": Palyak[3].SetActive(true); terulet = "Afrika"; break;
            case "Antarktika": Palyak[4].SetActive(true); terulet = "Antarktika"; break;
            case "DAmerika": Palyak[5].SetActive(true); terulet = "DAmerika"; break;
            case "KAzsia": Palyak[6].SetActive(true); terulet = "KAzsia"; break;
        }


        

    }
    public void UjJatek()
    {
        //itt már a pályaválasztóba kérjük le a kiválasztott pályát
        string name = EventSystem.current.currentSelectedGameObject.name;
        
        //kell a pályanév(a terület)
        switch (palyaNev)
        {
            //európa esetén
            case "Europa":
                switch (name)   //az alpálya neve
                {
                    //hogy ha az alpálya neve egyezik valamelyikkel akkor betöltjük a pályát ami kell illetve van 1 visszagomb
                    case "Level1": SceneManager.LoadScene("Level1"); break;
                    case "Level2": SceneManager.LoadScene("Level2"); break;
                    case "Level3": SceneManager.LoadScene("Level3"); break;
                    case "Level4": SceneManager.LoadScene("Level4"); break;
                    case "Level5": SceneManager.LoadScene("Level5"); break;
                    case "Vissza": Palyak[0].SetActive(false); break;               }
                break;
                //kb ez ismétlődik még párszor
            case "KAzsia":
                switch (name)
                {
                    case "Level11": SceneManager.LoadScene("Level11"); break;
                    case "Level12": SceneManager.LoadScene("Level12"); break;
                    case "Level13": SceneManager.LoadScene("Level13"); break;
                    case "Level14": SceneManager.LoadScene("Level14"); break;
                    case "Level15": SceneManager.LoadScene("Level15"); break;
                    case "Vissza":

                        for (int i = 0; i < Palyak.Length; i++)
                        {
                            Palyak[i].SetActive(false);
                        }
                        break;
                }
                break;

            case "EAmerika":
                switch (name)
                {
                    case "Palya1": SceneManager.LoadScene("Level21"); break;
                    case "Palya2": SceneManager.LoadScene("Level22"); break;
                    case "Palya3": SceneManager.LoadScene("Level23"); break;
                    case "Palya4": SceneManager.LoadScene("Level24"); break;
                    case "Palya5": SceneManager.LoadScene("Level25"); break;
                    case "Vissza":

                        for (int i = 0; i < Palyak.Length; i++)
                        {

                            Palyak[i].SetActive(false);

                        }
                        break;
                }
                break;

            case "Antarktika":
                switch (name)
                {
                    case "Palya1": SceneManager.LoadScene("Level31"); break;
                    case "Palya2": SceneManager.LoadScene("Level32"); break;
                    case "Palya3": SceneManager.LoadScene("Level33"); break;
                    case "Palya4": SceneManager.LoadScene("Level34"); break;
                    case "Palya5": SceneManager.LoadScene("Level35"); break;
                    case "Vissza":

                        for (int i = 0; i < Palyak.Length; i++)
                        {
                            Palyak[i].SetActive(false);
                        }
                        break;
                }
                break;

            case "DAmerika":
                switch (name)
                {
                    case "Palya1": SceneManager.LoadScene("Level41"); break;
                    case "Palya2": SceneManager.LoadScene("Level42"); break;
                    case "Palya3": SceneManager.LoadScene("Level43"); break;
                    case "Palya4": SceneManager.LoadScene("Level44"); break;
                    case "Palya5": SceneManager.LoadScene("Level45"); break;
                    case "Vissza":

                        for (int i = 0; i < Palyak.Length; i++)
                        {
                            Palyak[i].SetActive(false);
                        }
                        break;
                }
                break;

            case "Ausztralia":
                switch (name)
                {
                    case "Palya1": SceneManager.LoadScene("Level51"); break;
                    case "Palya2": SceneManager.LoadScene("Level52"); break;
                    case "Palya3": SceneManager.LoadScene("Level53"); break;
                    case "Palya4": SceneManager.LoadScene("Level54"); break;
                    case "Palya5": SceneManager.LoadScene("Level55"); break;
                    case "Vissza":

                        for (int i = 0; i < Palyak.Length; i++)
                        {

                            Palyak[i].SetActive(false);

                        }
                        break;
                }
                break;

            case "Afrika":
                switch (name)
                {
                    case "Palya1": SceneManager.LoadScene("Level61"); break;
                    case "Palya2": SceneManager.LoadScene("Level62"); break;
                    case "Palya3": SceneManager.LoadScene("Level63"); break;
                    case "Palya4": SceneManager.LoadScene("Level64"); break;
                    case "Palya5": SceneManager.LoadScene("Level65"); break;
                    case "Vissza":

                        for (int i = 0; i < Palyak.Length; i++)
                        {
                            Palyak[i].SetActive(false);
                        }
                        break;
                }
                break;
        }
        

    }

    public void Terkep()
    {
        //megkeressük az UI-n az inputfieldet ami kell
        GameObject inputField = GameObject.Find("PlayerNameInputField");
        //egy inputfieldből lekérjük a player nevét
        InputField inputom = inputField.GetComponent<InputField>();
        playerName = inputom.text;
       
        //ez a metódus majd az adatbázishoz vezet
        int kod=AddNev(inputom.text);

        //kodról később lényeg,hogy 0 esetén fut le
        if (kod == 0)
        {
            ujJatekos.SetActive(false);
            OnClick();
        }    


    }

    public void NevBekeres()
    {
        string name = EventSystem.current.currentSelectedGameObject.name;

        mainMenu.SetActive(false);
        ujJatekos.SetActive(true);

        if (name=="OK_Button")
        {
            SqlError.SetActive(false);
        }

        
    }

    public void Kilep()
    {
        try
        {
            Debug.Log(QualitySettings.GetQualityLevel());
            int pontok = GameManager.peldany.pontjaim;

            using (dbConnection = new SqliteConnection(connectionString))
            {

                dbConnection.Open();

                using (dbCmd = dbConnection.CreateCommand())
                {

                        string sqlQuery = String.Format("Update Mentett_Beallitasok Set MasterVolume={0}, Minoseg={1}, Szelesseg={2}, Magassag={3}", hangero, QualitySettings.GetQualityLevel(), Screen.width,Screen.height);
                        dbCmd.CommandText = sqlQuery;
                        dbCmd.ExecuteScalar();
                    
                    dbConnection.Close();
                }

            }
        }
        catch (Exception ex)
        {

            Debug.Log("Hibaüzi:" + ex.Message);
        }
        Application.Quit();

    }

    public void Options()
    {

        mainMenu.SetActive(false);
        beallitasok.SetActive(true);

    }

    public void Fomenu()
    {
        mainMenu.SetActive(true);
        beallitasok.SetActive(false);
        ujJatekos.SetActive(false);
        terkep.SetActive(false);
        betolto.SetActive(false);
    }

    public void SetFelbontas()
    {


        switch (dropDown.value)
        {
            case 1: Screen.SetResolution(640, 480, true); break;
            case 2: Screen.SetResolution(800, 600, true); break;
            case 3: Screen.SetResolution(1024, 768, true); break;
            case 4: Screen.SetResolution(1280, 720, true); break;
            case 5: Screen.SetResolution(1366, 768, true); break;
            case 6: Screen.SetResolution(1920, 1080, true); break;
        }
    }

    public void SetMaster(float ertek)
    {
        AudioListener.volume = ertek;
        hangeroSlider.value = ertek;
        hangero = ertek;
    }

    public void Minoseg()
    {        
        
        switch (dropDownMinoseg.value)
        {
            case 1: QualitySettings.SetQualityLevel(1,true);break;
            case 2: QualitySettings.SetQualityLevel(2,true);break;
            case 3: QualitySettings.SetQualityLevel(3,true);break;
            case 4: QualitySettings.SetQualityLevel(4,true);break;
            case 5: QualitySettings.SetQualityLevel(5,true);break;
        }
       
    }
    //sql huncutságok

    public void OnClick()
    {
        neveim.Clear();
        terkep.SetActive(true);
        betolto.SetActive(false);
        Debug.Log(pause);
        if(pause!=true)
            playerName = EventSystem.current.currentSelectedGameObject.name;

        Debug.Log(playerName);
        
        
        
        //létrehozzuk az adatbázis kapcsolatot
        using (dbConnection = new SqliteConnection(connectionString))
        {

            dbConnection.Open();

            using (dbCmd = dbConnection.CreateCommand())
            {
                //megadjuk az sql parancsunkat
                string sqlQuery = String.Format("SELECT JatekosNev,TeruletNev,Palya,Pont  FROM Eredmenyek WHERE JatekosNev='{0}'",playerName);
                dbCmd.CommandText = sqlQuery;

                using (IDataReader reader = dbCmd.ExecuteReader())
                {

                    //amíg olvasunk az adatbázisban szépen belerakjuk a listába a neveket
                    while (reader.Read())
                    {
                        neveim.Add(new Betolteshez(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(3)));
                        
                    }
                    reader.Close();
                    dbCmd.ExecuteScalar();
                    dbConnection.Close();

                }
                //ez azért kell, mert ha 1 mentett profilból visszalépünk a főmenübe akkor és utána új profilt csinálnuk ez kinullázza a térképet
                if(neveim.Count==0)
                    for (int i = 1; i < 6; i++)
                    {
                        Palyak[0].transform.GetChild(i).GetChild(1).GetChild(0).GetComponent<Text>().text = "0 Pont";
                        Palyak[1].transform.GetChild(i).GetChild(1).GetChild(0).GetComponent<Text>().text = "0 Pont";
                        Palyak[2].transform.GetChild(i).GetChild(1).GetChild(0).GetComponent<Text>().text = "0 Pont";
                        Palyak[3].transform.GetChild(i).GetChild(1).GetChild(0).GetComponent<Text>().text = "0 Pont";
                        Palyak[4].transform.GetChild(i).GetChild(1).GetChild(0).GetComponent<Text>().text = "0 Pont";
                        Palyak[5].transform.GetChild(i).GetChild(1).GetChild(0).GetComponent<Text>().text = "0 Pont";
                        Palyak[6].transform.GetChild(i).GetChild(1).GetChild(0).GetComponent<Text>().text = "0 Pont";
                    }

                //végigmegyünk a listán és lekérdezzük a dolgokat amik kellenek
                foreach (var item in neveim)
                {
                    
                    if (item.teruletNev == "Europa")
                    {
                        
                        for (int i = 1; i < 6; i++)
                        {
                            
                            
                            if (Palyak[0].transform.GetChild(i).name == item.palyaNev.ToString())
                            {
                                Palyak[0].transform.GetChild(i).GetChild(1).GetChild(0).GetComponent<Text>().text = item.pontszam.ToString()+" Pont";
                                Debug.Log(Palyak[0].transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text);

                            }
                        }
                       
                    }//és így tovább
                    else if (item.teruletNev == "EAmerika")
                    {
                        for (int i = 1; i < 6; i++)
                        {


                            if (Palyak[1].transform.GetChild(i).name == item.palyaNev.ToString())
                            {
                                Palyak[1].transform.GetChild(i).GetChild(1).GetChild(0).GetComponent<Text>().text = item.pontszam.ToString() + " Pont";
                                Debug.Log(Palyak[1].transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text);

                            }
                        }
                    }
                    else if (item.teruletNev == "Ausztralia")
                    {
                        for (int i = 1; i < 6; i++)
                        {


                            if (Palyak[2].transform.GetChild(i).name == item.palyaNev.ToString())
                            {
                                Palyak[2].transform.GetChild(i).GetChild(1).GetChild(0).GetComponent<Text>().text = item.pontszam.ToString() + " Pont";
                                Debug.Log(Palyak[2].transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text);

                            }
                        }
                    }
                    else if (item.teruletNev == "Afrika")
                    {
                        for (int i = 1; i < 6; i++)
                        {


                            if (Palyak[3].transform.GetChild(i).name == item.palyaNev.ToString())
                            {
                                Palyak[3].transform.GetChild(i).GetChild(1).GetChild(0).GetComponent<Text>().text = item.pontszam.ToString() + " Pont";
                                Debug.Log(Palyak[3].transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text);

                            }
                        }
                    }
                    else if (item.teruletNev == "Antarktika")
                    {
                        for (int i = 1; i < 6; i++)
                        {


                            if (Palyak[4].transform.GetChild(i).name == item.palyaNev.ToString())
                            {
                                Palyak[4].transform.GetChild(i).GetChild(1).GetChild(0).GetComponent<Text>().text = item.pontszam.ToString() + " Pont";
                                Debug.Log(Palyak[4].transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text);

                            }
                        }
                    }
                    else if (item.teruletNev == "DAmerika")
                    {
                        for (int i = 1; i < 6; i++)
                        {


                            if (Palyak[5].transform.GetChild(i).name == item.palyaNev.ToString())
                            {
                                Palyak[5].transform.GetChild(i).GetChild(1).GetChild(0).GetComponent<Text>().text = item.pontszam.ToString() + " Pont";
                                Debug.Log(Palyak[5].transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text);

                            }
                        }
                    }
                    else if (item.teruletNev == "KAzsia")
                    {
                        
                        for (int i = 1; i < 6; i++)
                        {

                            Debug.Log("ezmiez?"+Palyak[6].transform.GetChild(i).name);
                            if (Palyak[6].transform.GetChild(i).name == item.palyaNev.ToString())
                            {
                                Debug.Log("hahó2" + item.teruletNev);
                                Palyak[6].transform.GetChild(i).GetChild(1).GetChild(0).GetComponent<Text>().text = item.pontszam.ToString() + " Pont";
                                Debug.Log(Palyak[6].transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text);

                            }
                        }
                    }
                }
                

                

                foreach (var item in neveim)
                {
                    Debug.Log(item.nev + item.teruletNev + item.palyaNev + item.pontszam);
                    
                }
            }
        }
    }

   

    public void Beallitasok_betolto()
    {
       
        try
        {
            

            using (dbConnection = new SqliteConnection(connectionString))
            {

                dbConnection.Open();

                using (dbCmd = dbConnection.CreateCommand())
                {

                    string sqlQuery = String.Format("SELECT MasterVolume,Minoseg,Szelesseg,Magassag FROM Mentett_Beallitasok");
                    dbCmd.CommandText = sqlQuery;

                    using (IDataReader reader = dbCmd.ExecuteReader())
                    {
                        
                        
                            while (reader.Read())
                            {

                                hangero = reader.GetFloat(0);
                                QualitySettings.SetQualityLevel(reader.GetInt32(1), true);
                                Screen.SetResolution(reader.GetInt32(2), reader.GetInt32(3), true);
                            }
                            reader.Close();
                            dbCmd.ExecuteScalar();
                            dbConnection.Close();
                       
                    }
                   

                }

            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
        }

    }

    public void Betolto()
    {
        Debug.Log(connectionString);
        try
        {
            //lekérjük a képernyő magasságának 15%-át majd később kelleni fog
            int bPosY = (int)(Screen.height*0.15);
            mainMenu.SetActive(false);
            betolto.SetActive(true);            

            using (dbConnection = new SqliteConnection(connectionString))
            {
                
                dbConnection.Open();

                using (dbCmd = dbConnection.CreateCommand())
                {

                    string sqlQuery = String.Format("SELECT * FROM Jatekosok");
                    dbCmd.CommandText = sqlQuery;
                   
                    using (IDataReader reader = dbCmd.ExecuteReader())
                    {
                       
                        while (reader.Read())
                        {

                            //gombot inicializáljuk
                            GameObject probaButton = (GameObject)Instantiate(prefabButton);
                            //beállítjuk neki a szülő objektumát                            
                            probaButton.transform.SetParent(betolto.transform.GetChild(0).transform.GetChild(0).transform.GetChild(0));
                            //a localScale-t is beállítjuk
                            probaButton.transform.localScale = new Vector3(1, 1, 1);
                            //elhelyezzük a gombunkat
                            probaButton.transform.position = new Vector3((float)(Screen.width*0.5),(float)(Screen.height*0.5)+bPosY, 0);
                            //a neveket belerakjuk amint az sql utasításban lekérdeztünk
                            probaButton.transform.GetChild(0).GetComponent<Text>().text = reader.GetString(1);
                            //a gomb id-ját is így nevezzük el
                            probaButton.transform.name = reader.GetString(1);
                            //meghívjuk az Onclick metódust
                            probaButton.GetComponent<Button>().onClick.AddListener(OnClick);
                            bPosY -= (int)(Screen.height*0.1);
                            

                        }
                        reader.Close();
                        dbCmd.ExecuteScalar();
                        dbConnection.Close();

                    }
                    Debug.Log(betolto.transform.GetChild(0).GetChild(0).GetChild(0)); 

                }

        }
    }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
        }


    }

    private int AddNev(string name)
    {
        
        try
        {
            using (dbConnection = new SqliteConnection(connectionString))
            {

                dbConnection.Open();

                using (dbCmd = dbConnection.CreateCommand())
                {

                    string sqlQuery = String.Format("insert into Jatekosok(Nev) values ('{0}')", name);

                    dbCmd.CommandText = sqlQuery;
                    dbCmd.ExecuteScalar();
                    dbConnection.Close();



                }

            }

            return 0;
        }
        catch (SqliteException ex)
        {
            
            SqlError.SetActive(true);

            return 1;

        }
       

    }
}
