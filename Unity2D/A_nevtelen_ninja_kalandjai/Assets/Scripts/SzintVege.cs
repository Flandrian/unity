﻿using UnityEngine;


public class SzintVege : MonoBehaviour
{

    
    
    public string szintNev; //ez lesz a szint amire rámegyünk
    
    
    public void OnTriggerEnter2D(Collider2D masik)
    {

        if (masik.GetComponent<Player>() == null)
            return;

        
        LevelManager.peldany.KoviSzint(szintNev); //itt megyünk rá a kövi szintre
        
    }

    

}

