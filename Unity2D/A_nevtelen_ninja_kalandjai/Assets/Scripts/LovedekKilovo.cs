﻿using UnityEngine;
public class LovedekKilovo : MonoBehaviour
{
    public Transform cel;
    public UtasLovedek lovedek;
    public GameObject kilovesEffect;
    public float sebesseg;
    public float tuzgyorsasag;  //ilyen gyakran lövi ki a lövedéket
    public Animator animator;   //animációhoz
    public AudioClip lovesHang; //hanghoz

    private float _kovLovesMp;

    public void Start()
    {

        _kovLovesMp = tuzgyorsasag;

    }

    public void Update()
    {
        //amíg nem jár le az nem kell tulajdonképp semmit se csinálni
        if ((_kovLovesMp -= Time.deltaTime) > 0)
            return;
        //amennyiben lejár akkor ezt visszakell állítani
        _kovLovesMp = tuzgyorsasag;
        
        //létrehozzuk a lövedéket
        var _lovedek = (UtasLovedek)Instantiate(lovedek, transform.position, transform.rotation);
        //megadjuk a célját és a sebességét
        _lovedek.Init(cel, sebesseg);
        //effectezzük a kilövést
        if (kilovesEffect != null)
            Instantiate(kilovesEffect, transform.position, transform.rotation);
        //egy ágyuból löjük ki és az kap egy animációt azt játszuk le
        if (animator != null)
            animator.SetTrigger("Tuz");

        //hanghatás
        if (lovesHang != null)
            AudioSource.PlayClipAtPoint(lovesHang, transform.position);
                

    }
    //ezzel majd látjuk hova is lövünk
    public void OnDrawGizmos()
    {
        //ha nicns vélünk akkor ami ezután van fölösleges
        if (cel == null)
            return;

        Gizmos.color = Color.red;                           //vonal szine
        Gizmos.DrawLine(transform.position, cel.position);  //vonal rajzolás a kilövőtől a célunkig

    }

}
