﻿using UnityEngine;
using System.Collections;
//ez a class felelős azért hogy mutassa pl mennyi pontunk van
public class GameHud : MonoBehaviour
{
	
	public GUISkin Skin;  //editorból húzzuk majd be ezzel tudom a feliratnak a tulajdonságait állítani

	public void OnGUI()
	{

		GUI.skin = Skin;

		GUILayout.BeginArea (new Rect (0, 0, Screen.width, Screen.height));
		{

			GUILayout.BeginVertical (Skin.GetStyle ("GameHud"));
			{

				GUILayout.Label (string.Format ("Points {0}", GameManager.peldany.pontjaim), Skin.GetStyle ("TextPontok"));	//label ami a pontjaimat írja ki
				var time = LevelManager.peldany.futoIdo;													
				GUILayout.Label (string.Format ("{0:00}:{1:00} plusz {2} bonusz",									//ez a label a bonusz pontot írja ki valamint az időt
					time.Minutes + (time.Hours*60),
					time.Seconds,
					LevelManager.peldany.idoBonusz), Skin.GetStyle("TextIdo"));
                
			}
			GUILayout.EndVertical ();

		}
		GUILayout.EndArea ();

	}

}

