﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class HPBar : MonoBehaviour
{
    public Player player;  //a playert az inspectorba fogjuk hozzáragasztani
    public Transform foreGroundSprite;  //ez a hp bar maga
    public SpriteRenderer ForeGroundRenderer;       //a HP barnak a szinét megváltoztatni ha 0 lesz
    public Color MaxHPColor;
    public Color MinHPColor;

    public void Update()
    {

        var HPSzazalek = player.HP / (float)player.maxHP;              //százalékos hp

        foreGroundSprite.localScale = new Vector3(HPSzazalek, 1, 1);
        ForeGroundRenderer.color = Color.Lerp(MaxHPColor,MinHPColor,HPSzazalek);    //a két color között fog 1 színt visszaadni HPszazalék pedig hogy milyen léptékben

    }

}

