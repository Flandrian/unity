﻿using UnityEngine;
using System.Collections;

public class KontrollerAllas2D {

	public bool UtkozikBal { get; set;}		//bal oldala ütközik
	public bool UtkozikJobb { get; set;}	//jobb oldala ütközik
	public bool UtkozikLent { get; set;}	//lent ütközik
	public bool UtkozikFent { get; set;}	//fent ütközik
	public bool Foldon { get{return UtkozikLent;}}
	public bool Utkozes{get {return UtkozikBal || UtkozikJobb || UtkozikFent || UtkozikLent; }}// valamelyik esetén igaz lesz

	//total reset(alaphelyzetbe állítás)
	public void Reset()
	{
		UtkozikBal =
		UtkozikJobb =
		UtkozikFent =
		UtkozikLent = false;
	}
	

}
