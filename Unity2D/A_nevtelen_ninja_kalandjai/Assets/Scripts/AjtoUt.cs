﻿using UnityEngine;
using System.Collections.Generic;

public class AjtoUt : MonoBehaviour
{

    public Transform[] pontok; 
   
    //ezzel lesz majd ping-pong effektus
    public IEnumerator<Transform> KapottUtvonal(int kapcsolo)
    {

        if (pontok == null || pontok.Length < 1)        //ha kisebb a pontok tömböm elemszáma mint 1 akkor return(mert ha csak 2> pontom van a tömben nincs mivel összekötni) és még null esetén is
            yield break;

        var irany = 1;
        var index = 0;

        if (kapcsolo == 1)                  //irány és index meghatározás ha kapcsolo=1
        {
            irany = 1;
            index = 0;
        }

        else if (kapcsolo == 2)             //irány és index meghatározás ha kapcsolo=2
        {

            irany = -1;
            index = pontok.Length-1;

        } 

        
        while (true)
        {            
           
            yield return pontok[index];             //ez visszaadja a pontok[indexedik] elemét, de utána tovább megy
                        

            if (pontok.Length == 1)                 //ha a pontok tömbnek csak 1 eleme van akkor innentől kiugrunk a ciklusból
                continue;

            if(kapcsolo==2 && index > 0 && index <= pontok.Length-1)            //ebben esetben fog visszafelé indulni majd az objektum
            {
                index += irany;
               
            }
            else if(kapcsolo==1 && index < pontok.Length - 1 && index >= 0)     //ebben esetben fog előrefelé indulni majd az objektum
            {

                index += irany;

            }
                                            
        }

    }

    //hasonlóan működik mint a sima gui elemeknél a megjelenítés kivéve,hogy csak a scene view-ban látszódnak a kirajzolások(debug miatt is jó)
    public void OnDrawGizmos()
    {

        if (pontok == null || pontok.Length < 2)        //ha kisebb a pontok tömböm elemszáma mint 2 akkor return(mert ha csak 2> pontom van a tömben nincs mivel összekötni) és még null esetén is
            return;

        for (int i = 1; i < pontok.Length; i++)
        {

            Gizmos.DrawLine(pontok[i - 1].position, pontok[i].position);    //két pontot összeköt a drawline metódus(ugyebár ez csak a scene viewban lesz látható)

        }

    }
}
