﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CheckPoint : MonoBehaviour
{

    private List<IPlayerRespawnListener> _listeners;
    

    public void Awake()
    {

        _listeners = new List<IPlayerRespawnListener>();
       

    }

    public void CheckPointelhagyas()
    {


    }

    public void SpawnPlayer(Player player)
    {
        player.ReSpawn(transform);

        foreach (var listener in _listeners)
        {
            listener.OnPlayerRespawnCheckpoint(this, player);
        }

    }


    public void ObjektumCheckPointRendeles(IPlayerRespawnListener listener)
    {

        _listeners.Add(listener);

    }


    public void CheckPointTalalat()
    {
        StartCoroutine(CheckPointTalalatCo(GameManager.peldany.pontjaim+LevelManager.peldany.idoBonusz));

    }

    private IEnumerator CheckPointTalalatCo(int bonusz)
    {
        LebegoText.Mutat(string.Format("CheckPoint!"), "CheckPointText", new TextPozicioKozep(.5f));

        yield return new WaitForSeconds(.5f);
        LebegoText.Mutat(string.Format("+{0} pontok!", bonusz), "CheckPointText", new TextPozicioKozep(.5f));
    }


    
}
