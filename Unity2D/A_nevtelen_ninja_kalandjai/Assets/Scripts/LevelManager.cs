﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine.SceneManagement;
using System.Data;
using Mono.Data.Sqlite;

public class LevelManager : MonoBehaviour
{

	private List<CheckPoint> checkPointok;																//a checkpontotkat beteszem 1 listába
	private int aktualcheckPointIndex;																	//aktualis checkpoint indexe(a player melyik checkponznál van épp)
	private DateTime kezdo;
    private string connectionString = Menu.connectionString;
    private int mentettPontok;
	private int levettPontok;
    private IDbConnection dbConnection;
    private IDbCommand dbCmd;

    public CheckPoint debugSpawn;																		//teszteléshez
	public int bonuszViszaszamlal;																		//max bonusz bizonyos idő alatt
	public int bonuspontMp;																				//ennyivel csökken a bónuszpont mp-ként	

	public static LevelManager peldany { get; private set;}
	public Player player { get; private set;}
	public KameraKontroller kamera { get; private set;}
	public TimeSpan futoIdo { get{return DateTime.UtcNow - kezdo; }}
	public int idoBonusz
	{
		get
		{

			var megmaradtIdo = (int)(bonuszViszaszamlal - futoIdo.TotalSeconds);			//ezzel kiszámoljuk a megmaradt időt
			return Mathf.Max (0,megmaradtIdo)*bonuspontMp;									//ezzel pedig a pontokat amit lehet kapni	0-kulonbsegMp

		}

	}
    //A szint indításakor kellenek
	public void Awake()
	{
        Debug.Log(QualitySettings.GetQualityLevel());
        GameManager.peldany.Reset();    //totál nullázás(nehogy valamelyik szintről behúzzuk az ottani pontot)
        mentettPontok = GameManager.peldany.pontjaim;   //A reset miatt ez 0 lesz
		peldany = this; //erre a szintre vontakozik
       
	}

	public void Start()
	{
        
		checkPointok = FindObjectsOfType<CheckPoint>().OrderBy(t => t.transform.position.x).ToList();  //a CheckPoint tipusú objektumokat megkeresi és belerakja a listbe(x értéke szerint még sorba is rendezem)
        aktualcheckPointIndex = checkPointok.Count > 0 ? 0 : -1;  //ha több mint 0 elem van beene akkor az elsőre megyünk ha kisebb min nulla akkor -1(vagyis nincs benne)

		player = FindObjectOfType<Player>();  //megkeressük az adott objectet
		kamera = FindObjectOfType<KameraKontroller>();  //megkeressük az adott objectet

		kezdo = DateTime.UtcNow;    //aktuális idő lekérdezése

        var listeners = FindObjectsOfType<MonoBehaviour>().OfType<IPlayerRespawnListener>();

        foreach (var listener in listeners)
        {
            for (var i = checkPointok.Count - 1; i >= 0; i--)
            {
                var tav = ((MonoBehaviour)listener).transform.position.x - checkPointok[i].transform.position.x;

                if (tav < 0)
                    continue;

                checkPointok[i].ObjektumCheckPointRendeles(listener);
                break;

            }
        }


        //hogy ha van debugSpawn akkor spawnolja a playert viszont ha az indexünk nem -1 akkor az aktualis pontra tegye a playert
#if UNITY_EDITOR   //ez a rész csak editorba fog futni a az else-s fog standAlone menni
        if (debugSpawn != null)
			debugSpawn.SpawnPlayer (player);
		else if (aktualcheckPointIndex != -1)
			checkPointok [aktualcheckPointIndex].SpawnPlayer (player);

#else
		if(aktualcheckPointIndex != -1)
			checkPointok[aktualcheckPointIndex].SpawnPlayer(player);
		
#endif
	}

	public void Update()
	{

		var utolsoCheckPoint = aktualcheckPointIndex + 1 >= checkPointok.Count;		//ha az aktuális pont >= az össz chekpont számával akkor a lista végén vagyunk

		//ha elértük az utcsót nem kell csinálni semmit se
		if (utolsoCheckPoint)
			return;


		var koviCheckPointTav = checkPointok [aktualcheckPointIndex + 1].transform.position.x - player.transform.position.x;	//tavolsag lekérdezés, hogy milyen messze van a kövi checkPointtól a palyer

		//ha ez a távolság nagyobb mint 0 még nem kell csinálni semmit se
		if (koviCheckPointTav >= 0)
			return;


		checkPointok [aktualcheckPointIndex].CheckPointelhagyas ();		//amennyiben 0 lesz akkor függvény hívás hogy mi történjen
		aktualcheckPointIndex++;										//index növelés
		checkPointok[aktualcheckPointIndex].CheckPointTalalat();

		GameManager.peldany.PontotAd (idoBonusz);						//itt adjuk hozzá a bonuszpontokat
		mentettPontok = GameManager.peldany.pontjaim;
		kezdo = DateTime.UtcNow;

        

	}
    //1 Coroutine van benne(KoviSzintCo(palyaNev))
    public void KoviSzint(string palyaNev)
    {

        StartCoroutine(KoviSzintCo(palyaNev));

    }

    //1 Coroutine van benne(PlayerGyilkosCo())
    public void PlayerGyilkos()
	{

		StartCoroutine (PlayerGyilkosCo());

	}
    //A szint lezárása kiírások, pont mentés az adatbázisba, következő pálya betöltése
    private IEnumerator KoviSzintCo(string palyaNevem)
    {
        
        player.SzintVege();
        GameManager.peldany.PontotAd(idoBonusz);
        LebegoText.Mutat(string.Format("Teljesítetted a pályát!"), "CheckPointText", new TextPozicioKozep(.2f));
        yield return new WaitForSeconds(1f);
        
        LebegoText.Mutat(string.Format("{0} pont!", GameManager.peldany.pontjaim), "CheckPointText", new TextPozicioKozep(.15f));

        Mentes();

        yield return new WaitForSeconds(5f);

        if (string.IsNullOrEmpty(palyaNevem))
            SceneManager.LoadScene("StartScreen");
        else
            SceneManager.LoadScene(palyaNevem);

    }


	//ez történik ha meghal a player
	private IEnumerator PlayerGyilkosCo()
	{
		
		player.Gyilkos();										//gyilkolós metódus meghívása
		kamera.kovet = false;									//kamera ne kövessen
		yield return new WaitForSeconds (2f);					//kicsit várunk
			
		kamera.kovet = true;								//visszaállítjuk a követést

		//ha nem -1 az aktualis pont akkor visszarakatjuk a player az aktuális pontra
		if (aktualcheckPointIndex != -1)
			checkPointok [aktualcheckPointIndex].SpawnPlayer (player);


		kezdo = DateTime.UtcNow;//újrakérdezzük az időt a visszaszámolás miatt

		if(mentettPontok>0)
		{   // a halál miatt a pontok egy része elveszik
			levettPontok = (int)(mentettPontok * 0.1);
			GameManager.peldany.ResetPontok (mentettPontok-levettPontok);
			mentettPontok=mentettPontok-levettPontok;
		}

	}

    //adatbázisos huncutkodás
    //lementjük az aktuális játékost, a területet, a pályát amin elérte a pontot,illetve a pontokat
    private void Mentes()
    {
        bool talalat = false;  //majd arra kell, hogy a játékos akivel vagyunk, rendelkezik-e már pontokkal ezen a pályán(fontos mert nem mindegy, hogy Insert vagy Update-lesz az adatbázisban)
        Scene palya = SceneManager.GetActiveScene();  //aktív scene lekérdezés
        List<Betolteshez> neveim = new  List<Betolteshez>();
        
        try
        {
            int pontok = GameManager.peldany.pontjaim;

            using (dbConnection = new SqliteConnection(connectionString))
            {

                dbConnection.Open();

                using (dbCmd = dbConnection.CreateCommand())
                {

                    string sqlQuery = String.Format("SELECT JatekosNev,Palya FROM Eredmenyek");
                    dbCmd.CommandText = sqlQuery;

                    using (IDataReader reader = dbCmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            
                            neveim.Add( new Betolteshez(reader.GetString(0), reader.GetString(1)));
                            

                        }
                        reader.Close();
                        dbCmd.ExecuteScalar();
                        
                    }

                    foreach (var item in neveim)
                    {
                       
                        if (item.nev == Menu.playerName && item.palyaNev == palya.name)
                        {
                            talalat = true;
                            break;
                        }
                    }

                    Debug.Log(Menu.playerName);
                    

                    //hogyha nincs az emberünknek még pontja a pályán akkor Insert egyébként update lesz
                    if (talalat == false)
                    {
                        
                        sqlQuery = String.Format("insert into Eredmenyek(TeruletNev,Palya,Pont,JatekosNev) values ('{0}','{1}','{2}','{3}')", Menu.terulet, palya.name, pontok, Menu.playerName);

                        dbCmd.CommandText = sqlQuery;
                        dbCmd.ExecuteScalar();

                    }
                    else if (talalat == true)
                    {
                       
                        sqlQuery=String.Format("Update Eredmenyek Set Pont={0} Where Jatekosnev='{1}' and Palya='{2}' and Pont<'{0}'", pontok, Menu.playerName, palya.name);
                        dbCmd.CommandText = sqlQuery;
                        dbCmd.ExecuteScalar();
                    }
                    dbConnection.Close();
                }

            }
        }
        catch (Exception ex)
        {

            Debug.Log("Hibaüzi:"+ex.Message);
        }



    }
}

