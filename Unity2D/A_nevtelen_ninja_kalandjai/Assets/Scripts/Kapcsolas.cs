﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Kapcsolas : MonoBehaviour
{
    //hogyan kovesse az utvonalat
    public enum kovetestipus
    {

        PontFele

    }

    public kovetestipus tipus = kovetestipus.PontFele;              // követés tipusának default értéke
    public AjtoUt utvonal;                                          // maga az utvonal
    public float sebesseg = 1;                                      // maga az objektum sebessége
    public float maxtavCel = .1f;                                   // maximum táv amikor az adott pontról már továbbléphet 	
    public GameObject erreRakom;                                    // az a GameObject amire a script kerülni fog                                   
    public Animator animatorom;                                     // animator vezérlő


    private int _kapcsolo=0;                                        //animáció aktiválásához kell illetve ezt az értéket kapja meg az AjtoUt class KapottUtvonal metódusa is
    private IEnumerator<Transform> aktualisPont;                    
   

    

    //ezt a metódust a unity autómatikusan felébreszti

    public void Start()
    {
             

        //rögtön hibakezelés, ha a
        if (utvonal == null)
        {
            Debug.LogError("Az utvonal nem lehet null", gameObject);
            return;
        }


        

    }

    //bal egér kattintásra
    public void OnMouseDown()
    {       

        if (_kapcsolo == 0)
            _kapcsolo = 1;
        else if (_kapcsolo == 1)
            _kapcsolo = 2;
        else if (_kapcsolo == 2)
            _kapcsolo = 1;

        aktualisPont = utvonal.KapottUtvonal(_kapcsolo);                         //lekérjük az aktuális pontot úgy hogy megadjuk a kapcsolo értékét is

        aktualisPont.MoveNext();                                            //egyből léptetek is


        //ha ez van akkor nem megyünk tovább
        if (aktualisPont.Current == null)
            return;

        erreRakom.transform.position = aktualisPont.Current.position;             

    }

    public void Update()
    {

        
        //ha maga az aktuális pont null vagy a benne található pont null akkor return
        if (aktualisPont == null || aktualisPont.Current == null)
            return;
        
        //ha a követéstipus az hogy a pont felé megyünk akkor a Vector3-nak egy metódusát használva fog mozogni a platform
        if (tipus == kovetestipus.PontFele)
        {
            erreRakom.transform.position = Vector3.MoveTowards(erreRakom.transform.position,aktualisPont.Current.position, Time.deltaTime * sebesseg);
        }
        
        var tav = (erreRakom.transform.position - aktualisPont.Current.position).sqrMagnitude;
       
        if (tav < maxtavCel * maxtavCel)
            aktualisPont.MoveNext();

        animatorom.SetInteger("lekapcsolva", _kapcsolo);

    }

}



