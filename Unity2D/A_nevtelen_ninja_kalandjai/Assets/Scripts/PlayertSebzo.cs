﻿using System;
using System.Collections.Generic;
using UnityEngine;


class PlayertSebzo : MonoBehaviour
{
    //ha a player sebezve van akkor visszais lökődik és hp-t veszít

    public int sebzes = 10;

    //ezek itt a visszalökéshez kellenek
    private Vector2                     
            _utolsoPozicio,
            _sebesseg;

    public void LateUpdate()
    {
       
        _sebesseg = (_utolsoPozicio - (Vector2)transform.position) / Time.deltaTime;    //visszalökés sebessége
        _utolsoPozicio = transform.position; //lövedék utolsó pozíciója

    }

    public void OnTriggerEnter2D(Collider2D utkoz)
    {
       
        var player = utkoz.GetComponent<Player>();  //lekérjük a Player komponenseket

        if (player == null)                         //ha ütközünk, de nem playerrel akkor return
            return;

        
        player.Sebzes(sebzes,gameObject);

        var kontroll = player.GetComponent<KarakterKontroller2D>();
        var totalSebesseg = kontroll.sebesseg + _sebesseg;
        //itt van beállítva a visszalökés iránya, a visszalökés ereje x illetve y esetén is
        kontroll.SetEro(new Vector2(
            -1 * Mathf.Sign(totalSebesseg.x) * Mathf.Clamp(Mathf.Abs(totalSebesseg.x) * 6, 10, 40),     
            -1 * Mathf.Sign(totalSebesseg.y) * Mathf.Clamp(Mathf.Abs(totalSebesseg.y) * 6, 0, 25)));

       

    }


}

