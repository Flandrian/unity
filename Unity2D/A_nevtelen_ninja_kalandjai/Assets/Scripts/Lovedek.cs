﻿using UnityEngine;

public abstract class Lovedek : MonoBehaviour
{

  
    public float sebesseg;
    public LayerMask utkozoMask;
    public GameObject tulaj { get; private set; }
    public Vector2 irany { get; private set; }
    public Vector2 alapSeb { get; private set; }

    
    //Alapvető inicializálások
    public void Init(GameObject _tulaj, Vector2 _irany, Vector2 _alapSeb)
    {

        tulaj = _tulaj;
        irany = _irany;
        alapSeb = _alapSeb;
        
        
        OnInit();//későbbiekben valamelyik gyerekben lesz kifejtve

       
        //ez azért kell mer ha pl egy ellenséges egység lőtt akkor az általa kilőtt lovedék rossz irányba állt ezzel a pár sorral kilett küszöbölve
        if (tulaj.ToString() != "Player (UnityEngine.GameObject)")
        {
            if (irany.x < 0)
                transform.rotation = Quaternion.Euler(0, 0, 90);    //forgatásnak adom meg az értékét
            else
                transform.rotation = Quaternion.Euler(0, 0, -90); 
        }
    }

    protected virtual void OnInit()
    {

    }

    protected virtual void NincsUtkozes(Collider2D collide)
    {

    }

    protected virtual void tulajUtkozes()
    {

    }
    protected virtual void UtkozSebez(Collider2D collide, ISebzes sebzes)
    {
    
    }
    protected virtual void UtkozEgyeb(Collider2D collide)
    {
    
    }

    public virtual void OnTriggerEnter2D(Collider2D collide)
    {

        /*	Layerek leírása bitekben(példa)
					  Bináris	  Decimális
			Layer 0 = 0000 0001	= 1
			Layer 1 = 0000 0010	= 2
			Layer 2 = 0000 0100	= 4
			Layer 3 = 0000 1000	= 8
			Layer 4 = 0001 0000	= 16
			Layer 5 = 0010 0000	= 32
			Layer 6 = 0100 0000	= 64
			Layer 7 = 1000 0000	= 128

			collide.gameObject.layer = tulajdonképpa layerek számozása layer0,1,2,3...

			layer5 + layer2
			0010 	 0100
			Layer1+2+5+6
			0110	 0110

			layer mask = 0110 0110
			a Layer 5 benne van a maszkba

			(1<<5) "<<"jelenti,hogy az 1-est a binárisban 5-el balra csúsztatom
			0000 0001 << 5 = 0010 0000

			0110 0110
			&(and)
			0010 0000
			---------az & operátor csak azokkal a bitekkel tér vissza 1-el amik mindkét esetben 1esek
			0010 0000 vagyis ez benne van a maszkban

			0110 0110
			&
			1000 0000
			---------
			0000 0000 vagyis ez nincs benne a maszkban mer csupa 0
		*/

        if ((utkozoMask.value & (1 << collide.gameObject.layer)) == 0)
        {
            NincsUtkozes(collide);
            return;
        }

        var tulaje = collide.gameObject == tulaj;

        if (tulaje)
        {
            tulajUtkozes();
            return;
        }

        var sebzes = (ISebzes)collide.GetComponent(typeof(ISebzes));

        if (sebzes != null)
        {

            UtkozSebez(collide,sebzes);
            return;
        }

        UtkozEgyeb(collide);

    }

}

