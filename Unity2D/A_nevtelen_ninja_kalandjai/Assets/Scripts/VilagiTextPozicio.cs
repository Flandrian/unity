﻿using System;
using UnityEngine;


public class VilagiTextPozicio : ILebegoTextPozicio
{

    private readonly Camera _camera;
    private readonly Vector3 _vilagP;
    private readonly float _sebesseg;
    private float _lelep;
    private float _yOffset;

    //konstruktor a classhoz
    public VilagiTextPozicio(Camera camera, Vector3 vilagP,float lelep,float sebesseg)
    {
        _camera = camera;           //csak felhasználjuk megjelenítésre a fő kamerát
        _vilagP = vilagP;           //pl az a pozíció ahol a coin van
        _lelep = lelep;             //ennyi ideje van lelépni
        _sebesseg = sebesseg;       //ilyen gyorsan megy


}

    public bool GetPozicio(ref Vector2 pozicio, GUIContent content, Vector2 meret)
    {
        
        if ((_lelep -= Time.deltaTime) <= 0)    //ez fogja megmondani, hogy mikor kell majd a gameObjectet(FloatingText) elpusztítani
            return false;

        var screenPozicio = _camera.WorldToScreenPoint(_vilagP);
        pozicio.x = screenPozicio.x - (meret.x / 2);
        pozicio.y = Screen.height - screenPozicio.y - _yOffset;

        _yOffset += Time.deltaTime * _sebesseg;

        return true;
    }
}

