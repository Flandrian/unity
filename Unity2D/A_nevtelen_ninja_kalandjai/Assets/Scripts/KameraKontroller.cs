﻿using UnityEngine;
using System.Collections;

public class KameraKontroller : MonoBehaviour {


	private Vector3     //a kis világunk minimuma illetve maximuma lesz
		min, 
		max;

	public Transform player; //player objektumot rakjuk ide
	public Vector2 
		margo,
		simitas;

	public BoxCollider2D szelek;    // ide kerüla  kis világunk

	public bool kovet { get; set;} //ez lehet false is pl ha meghalunk


	//inicializalashoz 
	public void Start()
	{
	
		min = szelek.bounds.min;
		max = szelek.bounds.max;
		kovet = true;
	
	}

	public void Update()
	{

		Kovetes ();//kicsit szebb legyen


	}

	public void Kovetes()
	{
	
		var x = transform.position.x;
		var y = transform.position.y;


		if (kovet) 
		{
			
			if (Mathf.Abs (x - player.position.x) > margo.x)
				x = Mathf.Lerp (x, player.position.x, simitas.x * Time.deltaTime);

			if (Mathf.Abs (y - player.position.y) > margo.y)
				y = Mathf.Lerp (y, player.position.y, simitas.y * Time.deltaTime);
			
		}

		var kameraSizeSz = GetComponent<Camera> ().orthographicSize * ((float)Screen.width/Screen.height);

		x = Mathf.Clamp (x, min.x + kameraSizeSz, max.x - kameraSizeSz);
		y = Mathf.Clamp (y, min.y + GetComponent<Camera> ().orthographicSize, max.y - GetComponent<Camera> ().orthographicSize);

		transform.position = new Vector3 (x, y, transform.position.z);

	}



}
