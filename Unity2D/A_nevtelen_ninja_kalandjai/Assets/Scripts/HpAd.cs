﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class HpAd :MonoBehaviour, IPlayerRespawnListener
{

    public GameObject effect;                       //effect ami akkor lesz lejátszva, ha felvettük a hp-t
    public int pluszHp;                             //plusz HP mennyisége
    public int pluszStamina;                        //plusz stamina mennyisége
    public AudioClip hpHang;                        //hang hozzá


    //ha ütközünk jönnek a történések
    public void OnTriggerEnter2D(Collider2D collide)
    {

        var player = collide.GetComponent<Player>();    //lekérjük a player komponenseit

        if ( player == null)                            //ha nincs player akkor nincs mit csinálni
            return;

        if (hpHang != null)
            AudioSource.PlayClipAtPoint(hpHang, transform.position);

        player.AdHp(pluszHp, pluszStamina, gameObject); //meghívjuk azt a metódust ami majd a hp-t illetve a stamina-t hozzáadja a playerhez

        Instantiate(effect, transform.position, transform.rotation);
            
        gameObject.SetActive(false);                    //tulajdonképpen eltüntetjük az objektumot

       
            

    }

    public void OnPlayerRespawnCheckpoint(CheckPoint cPont, Player player)
    {
        gameObject.SetActive(true);                      //tulajdonképpen visszahozzuk az objektumot
    }
}

