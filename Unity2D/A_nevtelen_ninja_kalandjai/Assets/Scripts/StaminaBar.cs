﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class StaminaBar :MonoBehaviour
{

    public Player player;
    public Transform foreGroundSprite;
    public SpriteRenderer ForeGroundRenderer;       //a HP barnak a szinét megváltoztatni ha 0 lesz
    public Color MaxHPColor = new Color(255 / 255f, 63 / 255f, 63 / 255f);                //Így adok meg színeket a unity engone-nek
    public Color MinHPColor = new Color(12 / 255f, 42 / 255f, 21 / 255f);

    public void Update()
    {

        var staminaSzazalek = player.stamina / (float)player.maxStamina;              //százalékos hp

        foreGroundSprite.localScale = new Vector3(staminaSzazalek, 1, 1);
        ForeGroundRenderer.color = Color.Lerp(MaxHPColor, MinHPColor, staminaSzazalek);    //a két color között fog 1 színt visszaadni HPszazalék pedig hogy milyen léptékben

    }
}
